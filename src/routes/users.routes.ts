import { Router } from 'express';
import AppError from '../errors/AppError';
import { GetAllUsers } from '../services/UserServices';
import User from '../interfaces/User';

// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

const usersRouter = Router();

usersRouter.get('/', async (request, response) => {
  try {
    const result = await GetAllUsers();

    const users = (result.map(user => user._source) as unknown) as [User];

    if (request.query && request.query.address === 'suite') {
      return response
        .status(200)
        .json(
          users.filter((user: User) => user.address.suite.includes('Suite')),
        );
    }

    return response.json(
      users
        .map((user: User) => {
          return {
            name: user.name,
            email: user.email,
            company: user.company.name,
          };
        })
        .sort(function (a: any, b: any) {
          if (a.name < b.name) return -1;
          if (a.name > b.name) return 1;
          return 0;
        }),
    );
  } catch (error) {
    throw new AppError({ message: error.message, statusCode: 400 });
  }
});

usersRouter.get('/websites', async (_, response) => {
  try {
    const result = await GetAllUsers();

    const users = (result.map(user => user._source) as unknown) as [User];

    return response.status(200).json(users.map((user: User) => user.website));
  } catch (error) {
    throw new AppError({ message: error.message, statusCode: 400 });
  }
});

export default usersRouter;
