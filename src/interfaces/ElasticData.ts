import User from './User';

export default interface ElasticData {
  _index: string;
  _type: string;
  _id: string;
  _score: number;
  _source: User;
}
