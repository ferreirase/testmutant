/* eslint-disable import/prefer-default-export */
import AppError from '../errors/AppError';
import { getClient } from '../client/elasticsearch';
import ElasticData from '../interfaces/ElasticData';

export const GetAllUsers = async (): Promise<[ElasticData]> => {
  try {
    const { hits } = await getClient().search({
      index: 'users',
    });

    return (hits.hits as unknown) as [ElasticData];
  } catch (error) {
    throw new AppError({ message: error.message, statusCode: 400 });
  }
};
