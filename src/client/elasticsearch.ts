/* eslint-disable no-restricted-syntax */
/* eslint-disable import/prefer-default-export */
import elasticsearch from 'elasticsearch';
import axios from 'axios';
import AppError from '../errors/AppError';

// eslint-disable-next-line @typescript-eslint/no-var-requires
require('dotenv').config();

export const getClient = (): elasticsearch.Client => {
  const client = new elasticsearch.Client({
    host: 'http://elasticsearch',
    log: 'trace',
  });

  return client;
};

export const initializeElasticSearch = async (): Promise<void> => {
  const { data: users } = await axios.get(
    'https://jsonplaceholder.typicode.com/users',
  );

  for await (const user of users) {
    await getClient().index(
      {
        index: 'users',
        type: 'type_users',
        body: user,
      },
      err => {
        if (err) {
          throw new AppError({ message: err.message, statusCode: 400 });
        }
      },
    );
  }
};
